import os
from os.path import join, dirname
import discord
from discord.ext import commands
from dotenv import load_dotenv

dotenv_path = join(dirname(__file__), "discord.env")
load_dotenv(dotenv_path)
discord_token = os.environ["DISCORD_TOKEN"]

client = commands.Bot(command_prefix="$")

@client.event
async def on_ready():
    print("Adventure Bot is online!")

@client.command()
async def ping(ctx):
    await ctx.send('Pong!')

@client.command()
async def nani(ctx):
    await ctx.send("https://i.pinimg.com/originals/35/e1/16/35e1165a21fdaac6f3dc0af49510c471.gif")

client.run(discord_token)